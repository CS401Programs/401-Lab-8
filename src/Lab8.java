import java.util.*;
import java.io.*;

public class Lab8
{
	public static void main( String[] args)
	{
		if ( args.length<1) { System.out.println("FATAL ERROR: Missing expression on command line\nexample: java Lab8 3+13/5-16*3\n"); System.exit(0); }
		
		// Stolen directly from stackoverflow with just a few mods :)
		String expr= args[0];  // i.e. somethinig like "4+5-12/3.5-5.4*3.14"; 
		System.out.println( "expr: " + expr );
		ArrayList<String> operatorList = new ArrayList<String>();
		ArrayList<String> operandList = new ArrayList<String>();
		// StringTokenizer is like an infile and calling .hasNext() that splits on + - / or *
		StringTokenizer st = new StringTokenizer( expr,"+-*/", true );
		while (st.hasMoreTokens())
		{
			String token = st.nextToken();
			if ("+-/*".contains(token))
				operatorList.add(token);
			else
				operandList.add(token);
    		}
 		System.out.println("Operators:" + operatorList);
		System.out.println("Operands:" + operandList);
		
		double result = evaluate( operatorList, operandList );
		System.out.println("The expression: " + expr + " evalutes to " + result + "\n");
	} // END MAIN
	
	
	// ............................................................................................
	// Y O U  W R I T E  T H I S  M E T H O D  (WHCIH YOU MAY TRANSPLANT INTO SIMPLE CALC)
	// ............................................................................................
	
	// TAKES THE LIST Of OPERATORS ANd OPERANDS RETURNS RESULT AS A DOUBLE
	static double evaluate( ArrayList<String> operatorList, ArrayList<String> operandList)
	{	
		ArrayList<Double> operands = new ArrayList<Double>();
				
		for (String num : operandList)
			operands.add(Double.parseDouble(num));
		
			while (operatorList.contains("*") || operatorList.contains("/")) {
				for (int i = 0; i < operatorList.size(); i++) {
					if (operatorList.get(i).equals("*")) {
						operands.set(i, operands.get(i) * operands.get(i + 1));
						operands.remove(i + 1);
						operatorList.remove(i);
					}

					else if (operatorList.get(i).equals("/")) {
						operands.set(i, operands.get(i) / operands.get(i + 1));
						operands.remove(i + 1);
						operatorList.remove(i);
					}
				} 
			}
			
			while (operatorList.contains("+") || operatorList.contains("-")) {
				for (int i = 0; i < operatorList.size(); i++) {
					if (operatorList.get(i).equals("+")) {
						operands.set(i, operands.get(i) + operands.get(i + 1));
						operands.remove(i + 1);
						operatorList.remove(i);
					}
				
					else if (operatorList.get(i).equals("-")) {
						operands.set(i, operands.get(i) - operands.get(i + 1));
						operands.remove(i + 1);
						operatorList.remove(i);
					}
				}
			}
		// STEP #1	SUGGEST YOU COPY/CONVERT THE OPERANDS LIST INTO A LIST OF DOUBLES
	
		// 	NOW YOU HAVE AN ARRAYLIST OF STRINGS (OPERATORS) AND ANOTHER OF DOUBLES (OPERANDS)

		// FIRST PROCESS  ALL * and / operators FROM THE LIST

		// SECOND PROCESS  ALL + and - operators FROM THE LIST
		
		// return operands.get(0); // IT SHOULD BE THE ONLY THING LEFT IN OPERANDS
		return operands.get(0);  // just to make it compile. you should return the .get(0) of operands list
	}
} // END LAB8
